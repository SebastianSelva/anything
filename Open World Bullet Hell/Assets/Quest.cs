﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "Quest/Quest")]
public class Quest : ScriptableObject
{
    [SerializeField] string id;
    public string ID { get { return id; } }
    public string questName;
    public string difficulty;
    public int totalKillCount;
    public int CurrentKillCount;
    public string EnemeyName;

    public string Summery;

    public string reward;

    #if UNITY_EDITOR
    protected virtual void OnValidate()
    {
        string path = AssetDatabase.GetAssetPath(this);
        id = AssetDatabase.AssetPathToGUID(path);
    }
    #endif

    public virtual Quest GetCopy()
    {
        return this;
    }
    public string MakeSummery()
    {
        

        string progress = "Progress: " + CurrentKillCount + " / " + totalKillCount + " " + EnemeyName + " Killed";



        string summery = "Difficulty: " + difficulty + "\n" + "\n" +
                         progress + "\n" + "\n" +
                         Summery + "\n" + 
                         reward;

        return summery;
    }
	
}
