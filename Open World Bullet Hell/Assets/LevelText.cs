﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelText : MonoBehaviour
{

	void OnEnable()
    {
        this.gameObject.GetComponent<Text>().text = GameManager.PlayerInstance.GetComponent<Character>().Level.ToString();
    }
}
