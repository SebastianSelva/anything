﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class SaveStats 
{

    public float strength;
    public float defence;
    public float vitality;
    public float agility;

    public int health;
    public int maxHealth;
    public int xp;
    public int xpForNextLevel;

    public float x;
    public float y;
    public float z;


    public void GetValues()
    {
        Character chara = GameManager.PlayerInstance.GetComponent<Character>();
        strength = chara.Strength.BaseValue;
        defence = chara.Defence.BaseValue;
        vitality = chara.Vitality.BaseValue;
        agility = chara.Agility.BaseValue;

        health = chara.Health;
        maxHealth = chara.MaxHealth;
        xp = chara.Xp;
        xpForNextLevel = chara.XpForLevel;


        x = chara.transform.position.x;
        y = chara.transform.position.y;
        z = chara.transform.position.z;
        
    }

    public void SetValues()
    {
        Character chara = GameManager.PlayerInstance.GetComponent<Character>();
        chara.Strength.BaseValue = strength;
        chara.Defence.BaseValue = defence;
        chara.Vitality.BaseValue = vitality;
        chara.Agility.BaseValue = agility;

        chara.Health = health;
        chara.MaxHealth = maxHealth;
        chara.Xp = xp;
        chara.XpForLevel = xpForNextLevel;

        chara.gameObject.transform.SetPositionAndRotation(new Vector3(x,0,z) , Quaternion.identity);
    }
}
