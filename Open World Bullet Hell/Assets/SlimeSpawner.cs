﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeSpawner : MonoBehaviour
{

    public GameObject SlimePrefab;
    public GameObject CurrentSlime;
    public float SpawnTimer;
    public bool playerNear;
    void Awake()
    {
        SpawnSlime();
    }

    void Update()
    {
        if (CurrentSlime == null)
        {
            SpawnTimer -= Time.deltaTime;
            if (SpawnTimer <= 0)
            {
                SpawnSlime();
            }
        }

        if (playerNear == false && CurrentSlime != null)
            CurrentSlime.SetActive(false);
        else if (playerNear == true && CurrentSlime != null)
            CurrentSlime.SetActive(true);

    }


    public void SpawnSlime()
    {
        GameObject Slime = Instantiate(SlimePrefab, this.transform.position, Quaternion.identity);
        CurrentSlime = Slime;
        SpawnTimer = 60;
    }

    private void OnTriggerEnter(Collider Col)
    {
        if (Col.tag == "Activator")
        {
            playerNear = true;
        }

    }

    private void OnTriggerExit(Collider Col)
    {
        if (Col.tag == "Activator")
        {
            playerNear = false;
        }

    }
}
