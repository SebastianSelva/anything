﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class XpBar : MonoBehaviour {

    
    public Text xp;


    public void setXpBar(float sizeNormailized)
    {
        this.transform.localScale = new Vector3(sizeNormailized, this.transform.localScale.y, this.transform.localScale.z);
    }

    void OnEnable()
    {
        xp = gameObject.GetComponentInChildren<Text>();
        xp.text = "Xp: " + GameManager.PlayerInstance.GetComponent<Character>().Xp.ToString() + " / " + GameManager.PlayerInstance.GetComponent<Character>().XpForLevel.ToString();
        setXpBar((float)(GameManager.PlayerInstance.GetComponent<Character>().Xp) / (float)(GameManager.PlayerInstance.GetComponent<Character>().XpForLevel));
        
    }
}
