﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthBar : MonoBehaviour
{

    private Character chara;
    private void Start()
    {
        if ( chara == null)
        {
            chara = GameManager.PlayerInstance.GetComponent<Character>();
        }
    }


    public void setXpBar(float sizeNormailized)
    {
        if (sizeNormailized >= 0)
            this.transform.localScale = new Vector3(sizeNormailized, this.transform.localScale.y, this.transform.localScale.z);
        else
            this.transform.localScale = new Vector3(0, transform.localScale.y, transform.localScale.z);
    }

    private void Update()
    {
        setXpBar((float)chara.Health / (float)chara.MaxHealth);
    }

}
