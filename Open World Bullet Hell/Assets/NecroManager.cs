﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NecroManager : MonoBehaviour {

    public GameObject KillCountPanel;
    public List<GameObject> EnemiesToKill;
    public GameObject countPanel;
    public Text text;
    public GameObject NecroPreFab;
    public bool isBossSpawned;
    public List<GameObject> spawners;
    public GameObject wall;
    public GameObject necro;
    public bool waited = true;
    void Start()
    {
        wall.SetActive(false);
    }
	private void OnTriggerEnter(Collider colider)
    {
        if (colider.tag == "Player")
        {
            KillCountPanel.SetActive(true);
            StartCoroutine(SpawnerTimer());
            wall.SetActive(true);
        }

        if (colider.tag == "Enemy")
            EnemiesToKill.Add(colider.gameObject);

        if (colider.tag == "SkeletonSpawner")
            spawners.Add(colider.gameObject);
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "Player")
        {
            KillCountPanel.SetActive(false);
            wall.SetActive(false);
            if (necro != null)
                necro.GetComponent<EnemyStats>().resetHealth();
        }
        for (int i = 0; i < spawners.Count; i++)
        {
            spawners[i].gameObject.SetActive(true);
        }

       

    }


    public void DisplayLeft()
    {
        for (int i = 0; i < EnemiesToKill.Count; i++)
        {
            if (EnemiesToKill[i] == null)
                EnemiesToKill.RemoveAt(i);
        }

        text.text = "Enemies Left: " + EnemiesToKill.Count;
    }
    
    void Update()
    {
        if(isBossSpawned == false && waited == true)
        DisplayLeft();

        if (isBossSpawned == false && waited == true)
        {
            if (EnemiesToKill.Count == 0)
            {
                GameObject boss =  Instantiate(NecroPreFab, this.transform.position, Quaternion.identity);
                necro = boss;
                isBossSpawned = true;
                waited = false;
                StopCoroutine(SpawnerTimer());
                
            }
        }

        if (isBossSpawned == true)
            KillCountPanel.SetActive(false);
        

        if(isBossSpawned == true)
        {
            waited = false;
            if (necro == null)
            {
                isBossSpawned = false;
                StartCoroutine(BossKilledTimer());
            }
        }
    }

    IEnumerator SpawnerTimer()
    {
        for (int i = 0; i < spawners.Count; i++)
        {
            spawners[i].gameObject.SetActive(false);
        }
        yield return new WaitForSeconds(120);
        for (int i = 0; i < spawners.Count; i++)
        {
            spawners[i].gameObject.SetActive(true);
        }
        StartCoroutine(SpawnerTimer());
    }
    IEnumerator BossKilledTimer()
    {

        yield return new WaitForSeconds(120);
        waited = true;
    }

}
