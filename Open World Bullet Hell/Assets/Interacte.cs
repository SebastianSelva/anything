﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VIDE_Data;
public class Interacte : MonoBehaviour {

    public bool playerNear;
    public GameObject InteractCanvas;


    void Awake()
    {
        InteractCanvas.SetActive(false);
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            InteractCanvas.SetActive(true);
            playerNear = true;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            InteractCanvas.SetActive(false);
            playerNear = false;
            VD.EndDialogue();
            GameManager.UI.container_NPC.SetActive(false);
            GameManager.UI.container_PLAYER.SetActive(false);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && playerNear == true)
        {
            if (!VD.isActive)
            {
               GameManager.UI.Begin(this.GetComponent<VIDE_Assign>());

            }
            else
            {
                if (VD.nodeData.isEnd)
                {
                    Debug.Log("Dialogue ended");
                    VD.EndDialogue();
                    GameManager.UI.container_NPC.SetActive(false);
                    GameManager.UI.container_PLAYER.SetActive(false);
                }
                else
                    VD.Next();
            }
        }
    }

    //NPC Dialogue Setter bool

  


}
