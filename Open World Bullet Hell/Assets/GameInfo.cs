﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class GameInfo  
{
   [System.NonSerialized] public GameObject King;
   [System.NonSerialized] public GameObject Steward;

    private static bool notFirstTime;

    public static bool NotFirstTime
    {
        get
        {
            return notFirstTime;
        }

        set
        {
            notFirstTime = value;
        }
    }


    //King 
    public int KingOverride;
    public bool KingFisrtTime;

    //Steward
    public int StewardOverride;
    public bool StewardSecondDialogue;

    //Quests
    public int SlimeQuestKillCount;
    public int GoblinQuestKillCount;
    public int OrcQuestKillCount;
    public int NecroKillCount;

    public bool IsOnSlimeQuest;
    public bool IsOnGoblinQuest;
    public bool IsOnOrcQuest;
    public bool IsOnNecroQuest;



    public void load()
    {
        GameManager.gameInfo.KingOverride = this.KingOverride;
        GameManager.gameInfo.KingFisrtTime = this.KingFisrtTime;

        GameManager.gameInfo.StewardOverride = this.StewardOverride;
        GameManager.gameInfo.StewardSecondDialogue = this.StewardSecondDialogue;

        GameManager.gameInfo.IsOnSlimeQuest = IsOnSlimeQuest;
        GameManager.gameInfo.IsOnGoblinQuest = IsOnGoblinQuest;
        GameManager.gameInfo.IsOnOrcQuest = IsOnOrcQuest;
        GameManager.gameInfo.IsOnNecroQuest = IsOnNecroQuest;

    }

    public void PrepareForSaveing()
    {
        KingOverride = King.GetComponentInChildren<VIDE_Assign>().overrideStartNode;
        StewardOverride = Steward.GetComponentInChildren<VIDE_Assign>().overrideStartNode;

        

    }

    public void Saving(GameInfo save)
    {
        IsOnSlimeQuest = save.IsOnSlimeQuest;
        IsOnGoblinQuest = save.IsOnGoblinQuest;
        IsOnOrcQuest = save.IsOnOrcQuest;
        IsOnNecroQuest = save.IsOnNecroQuest;

        KingOverride = save.KingOverride;
        KingFisrtTime = save.KingFisrtTime;

        StewardOverride = save.StewardOverride;
        StewardSecondDialogue = save.StewardSecondDialogue;

        SlimeQuestKillCount = save.SlimeQuestKillCount;
        GoblinQuestKillCount = save.GoblinQuestKillCount;
        OrcQuestKillCount = save.OrcQuestKillCount;
        NecroKillCount = save.NecroKillCount;
        
    }

    public void SetNPC()
    {
        King = GameManager.GM.King;
        Steward = GameManager.GM.Steward;
    }

    public void SetOverrides()
    {
        King.GetComponentInChildren<VIDE_Assign>().overrideStartNode = KingOverride;
        Steward.GetComponentInChildren<VIDE_Assign>().overrideStartNode = StewardOverride;
    }
}
