﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PlayerController : MonoBehaviour {

    private float HorizontalInput;
    private float VerticalInput;
    public float speed;
    
    public GameObject Render;
    public Animator animator;
    private Camera Cam;
    private Vector3 Movement;
    public float shotSpeed;
    public Vector3 AimDirection;
    public GameObject ShotDirector;
    public GameObject shot;
    public bool canMove = true;
    public Tilemap tilemap;
    private ParticleSystem ps;
    public Character character;
    public Sprite current;
    private float attackTimer;
    public float attackspeed= 0.8f;
    public bool isDead = false;
    public bool isDashing;
    public bool canDash;
    public const float maxDashTime = 1.0f;
    public float dashDistance = 15;
    public float dashStoppingSpeed = 0.1f;
    float currentDashTime = maxDashTime;
    float dashSpeed = 6;

    void Start ()
    {
        character = GetComponent<Character>();
        animator = Render.GetComponent<Animator>();
        ps = Render.GetComponent<ParticleSystem>();
        canDash = true;
        
        Cam = Camera.main;
	}

	void Update ()
    {

        attackTimer -= Time.deltaTime;

        
        HorizontalInput = Input.GetAxisRaw("Horizontal");
        VerticalInput = Input.GetAxisRaw("Vertical");


        //Remove Comment for animation
        animator.SetFloat("Rotation", ShotDirector.transform.rotation.eulerAngles.y);

        Render.transform.LookAt(Cam.transform);
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 5.23f;

        Vector3 objectPos = Camera.main.WorldToScreenPoint(ShotDirector.transform.position);
        mousePos.x = mousePos.x - objectPos.x;
        mousePos.y = mousePos.y - objectPos.y;

        float angle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;
        ShotDirector.transform.rotation = Quaternion.Euler(new Vector3(0, -angle,0));

        if (Input.GetKey(KeyCode.Mouse0) && character.EquipmentPanel.EquipmentSlots[2].Item != null)
        {

            if (attackTimer <= 0)
            {
                GameObject newShot = Instantiate(shot, ShotDirector.transform.position, Quaternion.Euler(new Vector3(0, -angle, 0)));
                newShot.GetComponent<Shot>().velocity = -newShot.transform.right * shotSpeed;
                newShot.transform.Rotate(90, newShot.transform.rotation.y, newShot.transform.rotation.z);
                attackTimer = attackspeed;
            }

            
        }

        if (Input.GetButtonDown("Fire2") && character.Stamina>25 && canDash) //Right mouse button
        {
            currentDashTime = 0;
            character.Stamina -= 25;
        }
        if (currentDashTime < maxDashTime)
        {
            canMove = false;
            isDashing = true;
            gameObject.transform.Translate(-ShotDirector.transform.right * dashDistance * Time.deltaTime);
            currentDashTime += dashStoppingSpeed;
        }
        else
        {
            canMove = true;
            isDashing = false;
        }


        if (isDead == false)
        {
            Vector3 move = new Vector3(-HorizontalInput, 0f, -VerticalInput).normalized;
            speed = (float)(4 + 5.6 * (GameManager.PlayerInstance.GetComponent<Character>().Agility.Value / 75));
            this.gameObject.transform.position += move * speed * Time.deltaTime;
        }
    }

    void FixedUpdate()
    {
        //TileSpeed();
        /*
        if(Input.GetKey(KeyCode.W) && canMove == true)
        {
            gameObject.transform.Translate((Vector3.forward * -VerticalInput).normalized * speed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.A) && canMove == true)
        {
            gameObject.transform.Translate((Vector3.right * -HorizontalInput).normalized * speed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.S) && canMove == true)
        {
            gameObject.transform.Translate((Vector3.back * VerticalInput).normalized * speed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.D) && canMove == true)
        {
            gameObject.transform.Translate((Vector3.left * HorizontalInput).normalized * speed * Time.deltaTime);
        }
        */

        


    }

    public void TileSpeed()
    {
        string tileName = tilemap.GetTile(new Vector3Int((int)this.gameObject.transform.position.x, 0, (int)this.gameObject.transform.position.y)).name;

        Debug.Log(tileName);

    }

    public void PlayerDamage(float Min, float Max, float AttackStat)
    {
        float damageMultiplier = 0.5f + AttackStat / 50;
        float damage = (Random.Range(Min, Max) * damageMultiplier);
        float minDamage = damage * 0.85f;
        float finalDamage;
        if (damage - character.Defence.Value <= minDamage)
        {
            finalDamage = minDamage;
        }
        else
        {
            finalDamage = damage - character.Defence.Value;
        }
        
        character.Health -= (int)Mathf.Round(finalDamage);
        
        
    }

   
}
