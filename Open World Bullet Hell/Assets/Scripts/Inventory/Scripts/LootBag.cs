﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootBag : MonoBehaviour
{

    [SerializeField] public List<Item> Items;

    public void AddItem(Item item)
    {
        Items.Add(item);
    }

    public void RemoveItem(Item item)
    {
        Items.Remove(item);
    }
	

    public void ClearItems()
    {
        Items.Clear(); 
    }
    
    void Update()
    {
        if(Items.Count == 0)
        {
            
            Destroy(this.gameObject);
        }
    }
	
}
