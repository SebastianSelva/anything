﻿using UnityEngine;

[CreateAssetMenu(menuName = "Item Effects/Heal")]
public class HealItemEffect : UsableItemEffect
{
	public int HealAmount;
    public bool ifHeathPot;

	public override void ExecuteEffect(UsableItem usableItem, Character character)
	{
        if (ifHeathPot)
        {
            character.Health += HealAmount;
            if (character.Health > character.MaxHealth)
                character.Health = character.MaxHealth;
        }
        else
        {
            character.Stamina += HealAmount;
            if (character.Stamina > character.MaxStamina)
                character.Stamina = character.MaxStamina;
        }
    }

	public override string GetDescription()
	{
        if(ifHeathPot)
		    return "Heals for " + HealAmount + " health.";
        else
            return "Increases Stamina by " + HealAmount;
	}
}
