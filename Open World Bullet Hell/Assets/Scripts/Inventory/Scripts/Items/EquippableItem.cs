﻿using UnityEngine;
using Kryz.CharacterStats;

public enum EquipmentType
{
	Helmet,
	Chest,
	Ring,
	Weapon,
	
}

[CreateAssetMenu(menuName = "Items/Equippable Item")]
public class EquippableItem : Item
{
	public int StrengthBonus;
	public int AgilityBonus;
    public int DefenceBonus;
	public int VitalityBonus;
	[Space]
	public float StrengthPercentBonus;
	public float AgilityPercentBonus;
	
	public float VitalityPercentBonus;
	[Space]
	public EquipmentType EquipmentType;

	public override Item GetCopy()
	{
		return Instantiate(this);
	}

	public override void Destroy()
	{
		DestroyImmediate(this);
	}

	public virtual void Equip(Character c)
	{
		if (StrengthBonus != 0)
			c.Strength.AddModifier(new StatModifier(StrengthBonus, StatModType.Flat, this));

		if (AgilityBonus != 0)
			c.Agility.AddModifier(new StatModifier(AgilityBonus, StatModType.Flat, this));

        if (DefenceBonus != 0)
            c.Defence.AddModifier(new StatModifier(DefenceBonus, StatModType.Flat, this));

		if (VitalityBonus != 0)
			c.Vitality.AddModifier(new StatModifier(VitalityBonus, StatModType.Flat, this));

		if (StrengthPercentBonus != 0)
			c.Strength.AddModifier(new StatModifier(StrengthPercentBonus, StatModType.PercentMult, this));

		if (AgilityPercentBonus != 0)
			c.Agility.AddModifier(new StatModifier(AgilityPercentBonus, StatModType.PercentMult, this));
		
		if (VitalityPercentBonus != 0)
			c.Vitality.AddModifier(new StatModifier(VitalityPercentBonus, StatModType.PercentMult, this));
	}

	public virtual void Unequip(Character c)
	{
		c.Strength.RemoveAllModifiersFromSource(this);
        c.Defence.RemoveAllModifiersFromSource(this);
		c.Agility.RemoveAllModifiersFromSource(this);
		c.Vitality.RemoveAllModifiersFromSource(this);
	}

	public override string GetItemType()
	{
		return EquipmentType.ToString();
	}

	public override string GetDescription()
	{
		sb.Length = 0;
		AddStat(StrengthBonus, "Strength");
		AddStat(AgilityBonus, "Agility");
        AddStat(DefenceBonus, "Defence");
		AddStat(VitalityBonus, "Vitality");

		AddStat(StrengthPercentBonus, "Strength", isPercent: true);
		AddStat(AgilityPercentBonus, "Agility", isPercent: true);
		
		AddStat(VitalityPercentBonus, "Vitality", isPercent: true);

		return sb.ToString();
	}

	protected void AddStat(float value, string statName, bool isPercent = false)
	{
		if (value != 0)
		{
			if (sb.Length > 0)
				sb.AppendLine();

			if (value > 0)
				sb.Append("+");

			if (isPercent) {
				sb.Append(value * 100);
				sb.Append("% ");
			} else {
				sb.Append(value);
				sb.Append(" ");
			}
			sb.Append(statName);
		}
	}
}
