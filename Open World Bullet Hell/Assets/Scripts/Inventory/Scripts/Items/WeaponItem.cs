﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kryz.CharacterStats;


[CreateAssetMenu(menuName = "Items/Equippable Item/Weapon")]
public class WeaponItem : EquippableItem
{

    public Sprite ShotSprite;
    public int DamageMinimum;
    public int DamageMaximum;
    public int WeaponSpeed;
    public float WeaponRange;


    public override void Equip(Character c)
    {
        if (StrengthBonus != 0)
            c.Strength.AddModifier(new StatModifier(StrengthBonus, StatModType.Flat, this));
        if (AgilityBonus != 0)
            c.Agility.AddModifier(new StatModifier(AgilityBonus, StatModType.Flat, this));
        
        if (VitalityBonus != 0)
            c.Vitality.AddModifier(new StatModifier(VitalityBonus, StatModType.Flat, this));

        if (StrengthPercentBonus != 0)
            c.Strength.AddModifier(new StatModifier(StrengthPercentBonus, StatModType.PercentMult, this));
        if (AgilityPercentBonus != 0)
            c.Agility.AddModifier(new StatModifier(AgilityPercentBonus, StatModType.PercentMult, this));
        
        if (VitalityPercentBonus != 0)
            c.Vitality.AddModifier(new StatModifier(VitalityPercentBonus, StatModType.PercentMult, this));

        if (DamageMinimum != 0)
            c.MinimumDamage = DamageMinimum;
        if (DamageMaximum != 0)
            c.MaximumDamage = DamageMaximum;
        if (ShotSprite != null)
            c.ShotSprite = ShotSprite;
        if (WeaponSpeed != 0)
            c.ShotSpeed = WeaponSpeed;
        if (WeaponRange != 0)
            c.Range = WeaponRange;

    }

    public override void Unequip(Character c)
    {
        c.Strength.RemoveAllModifiersFromSource(this);
        c.Agility.RemoveAllModifiersFromSource(this);
        
        c.Vitality.RemoveAllModifiersFromSource(this);
        c.MinimumDamage = 0;
        c.MaximumDamage = 0;
        c.ShotSprite = null;
        c.ShotSpeed = 0;
        c.Range = 0;
    }

    public override string GetDescription()
    {
        sb.Length = 0;
        AddTwoStat(DamageMaximum, DamageMinimum, "Damage");
        AddStat(StrengthBonus, "Strength");
        AddStat(AgilityBonus, "Agility");
      
        AddStat(VitalityBonus, "Vitality");

        AddStat(StrengthPercentBonus, "Strength", isPercent: true);
        AddStat(AgilityPercentBonus, "Agility", isPercent: true);
        
        AddStat(VitalityPercentBonus, "Vitality", isPercent: true);

        return sb.ToString();
    }

    protected void AddTwoStat(float maxValue, float minValue, string statName, bool isPercent = false)
    {
        if (minValue != 0)
        {
            if (sb.Length > 0)
                sb.AppendLine();

           

            if (isPercent)
            {
                //sb.Append(value * 100);
                //sb.Append("% ");
                //rethink life 
            }
            else
            {
                sb.Append(minValue);
                sb.Append(" - ");
                sb.Append(maxValue);
                sb.Append(" ");
            }
            sb.Append(statName);
        }
    }

}
