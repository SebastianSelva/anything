﻿using UnityEngine;
using UnityEngine.UI;
using Kryz.CharacterStats;
using System;
using System.Linq;
using System.Collections.Generic;

[System.Serializable]
public class Character : MonoBehaviour
{
    public int Health = 100;
    public int MaxHealth = 100;
    public int Stamina = 100;
    public int MaxStamina = 100;
    public Transform respawnPoint;
    public int Xp = 0;
    public int XpForLevel = 100;
    public int Level = 1;

    [Header("Stats")]
    public CharacterStat Strength;
    public CharacterStat Defence;
    public CharacterStat Agility;
    public CharacterStat Vitality;
    [Space]
    public int MinimumDamage;
    public int MaximumDamage;
    public int ShotSpeed;
    public float Range;
    public Sprite ShotSprite;

    [Header("Public")]
    public Inventory Inventory;
    public EquipmentPanel EquipmentPanel;
    public PlayerController playerController;
    public GameObject DeathPanel;
    public InventoryInput inventoryInput;
    [Header("Serialize Field")]
    [SerializeField] CraftingWindow craftingWindow;
    [SerializeField] StatPanel statPanel;
    [SerializeField] ItemTooltip itemTooltip;
    [SerializeField] Image draggableItem;
    [SerializeField] DropItemArea dropItemArea;
    [SerializeField] QuestionDialog reallyDropItemDialog;
    [SerializeField] ItemSaveManager itemSaveManager;
    [SerializeField] LootPickUp lootPickUp;
    [SerializeField] LootViewer lootViewer;

    private bool isDead;

	private BaseItemSlot dragItemSlot;

    public GameObject LevelUpPanel;

	private void OnValidate()
	{
		if (itemTooltip == null)
			itemTooltip = FindObjectOfType<ItemTooltip>();
	}

	private void Awake()
	{
		statPanel.SetStats(Strength, Defence, Agility, Vitality);
		statPanel.UpdateStatValues();
        if (LevelUpPanel.activeSelf == true)
            LevelUpPanel.SetActive(false);
		// Setup Events:
		// Right Click
		Inventory.OnRightClickEvent += InventoryRightClick;
		EquipmentPanel.OnRightClickEvent += EquipmentPanelRightClick;
		// Pointer Enter
		Inventory.OnPointerEnterEvent += ShowTooltip;
		EquipmentPanel.OnPointerEnterEvent += ShowTooltip;
		craftingWindow.OnPointerEnterEvent += ShowTooltip;
		// Pointer Exit
		Inventory.OnPointerExitEvent += HideTooltip;
		EquipmentPanel.OnPointerExitEvent += HideTooltip;
		craftingWindow.OnPointerExitEvent += HideTooltip;
		// Begin Drag
		Inventory.OnBeginDragEvent += BeginDrag;
		EquipmentPanel.OnBeginDragEvent += BeginDrag;
		// End Drag
		Inventory.OnEndDragEvent += EndDrag;
		EquipmentPanel.OnEndDragEvent += EndDrag;
		// Drag
		Inventory.OnDragEvent += Drag;
		EquipmentPanel.OnDragEvent += Drag;
		// Drop
		Inventory.OnDropEvent += Drop;
		EquipmentPanel.OnDropEvent += Drop;
		dropItemArea.OnDropEvent += DropItemOutsideUI;
	}
    void Start()
    {
        playerController = gameObject.GetComponent<PlayerController>();

        if (isDead == false)
        {
            InvokeRepeating("vitality", 0f, 1f);
            InvokeRepeating("StaminaIncrease", 0f, 1f);
        }
    }
	void Update()
    {
        if(Xp >= XpForLevel)
        {
            LevelUp();
        }

        if(Health <= 0)
        {
            isDead = true;
            inventoryInput.canInput = false;
            DeathPanel.SetActive(true);
            playerController.isDead = true;
            playerController.canDash = false;
        }
    }



    public void Death()
    {
        gameObject.transform.position = respawnPoint.position;
        Health = MaxHealth;
        isDead = false;
        inventoryInput.canInput = true;
        playerController.isDead = false;

        if (Level > 1)
        {
            Level -= 1;
            Strength.BaseValue--;
            Defence.BaseValue--;
            Agility.BaseValue--;
            Vitality.BaseValue--;
            Xp = 0;
            
        }
        DeathPanel.SetActive(false);
    }

    void vitality()
    {
        if (Health < MaxHealth)
        {
            Health += (int)(1 + 0.24f * Vitality.Value);
            if (Health > MaxHealth)
                Health = MaxHealth;
        }
    }
    void StaminaIncrease()
    {
        if (Stamina < MaxStamina)
        {
            Stamina += (int)(1 + 0.48f * Agility.Value);
            if (Stamina > MaxStamina)
                Stamina = MaxStamina;
        }
    }

    void LevelUp()
    {
        StartCoroutine(LevelUpPanelShow());
        Strength.BaseValue++;
        Defence.BaseValue++;
        Agility.BaseValue++;
        Vitality.BaseValue++;
        Xp = 0;
        XpForLevel += 100;
        Level++;
    }

    public System.Collections.IEnumerator LevelUpPanelShow()
    {
        LevelUpPanel.SetActive(true);
        yield return new WaitForSeconds(2);
        LevelUpPanel.SetActive(false);
    }

	private void OnDestroy()
	{
		if (itemSaveManager != null)
		{
			itemSaveManager.SaveEquipment(this);
			itemSaveManager.SaveInventory(this);
		}
	}

	private void InventoryRightClick(BaseItemSlot itemSlot)
	{
		if (itemSlot.Item is EquippableItem)
		{
			Equip((EquippableItem)itemSlot.Item);
		}
		else if (itemSlot.Item is UsableItem)
		{
			UsableItem usableItem = (UsableItem)itemSlot.Item;
			usableItem.Use(this);

			if (usableItem.IsConsumable)
			{
				Inventory.RemoveItem(usableItem);
				usableItem.Destroy();
			}
		}
	}

	private void EquipmentPanelRightClick(BaseItemSlot itemSlot)
	{
		if (itemSlot.Item is EquippableItem)
		{
			Unequip((EquippableItem)itemSlot.Item);
		}
	}

	private void ShowTooltip(BaseItemSlot itemSlot)
	{
		if (itemSlot.Item != null)
		{
			itemTooltip.ShowTooltip(itemSlot.Item);
		}
	}

	private void HideTooltip(BaseItemSlot itemSlot)
	{
		if (itemTooltip.gameObject.activeSelf)
		{
			itemTooltip.HideTooltip();
		}
	}

	private void BeginDrag(BaseItemSlot itemSlot)
	{
		if (itemSlot.Item != null)
		{
			dragItemSlot = itemSlot;
			draggableItem.sprite = itemSlot.Item.Icon;
			draggableItem.transform.position = Input.mousePosition;
			draggableItem.gameObject.SetActive(true);
		}
	}

	private void Drag(BaseItemSlot itemSlot)
	{
		draggableItem.transform.position = Input.mousePosition;
	}

	private void EndDrag(BaseItemSlot itemSlot)
	{
		dragItemSlot = null;
		draggableItem.gameObject.SetActive(false);
	}

	private void Drop(BaseItemSlot dropItemSlot)
	{
		if (dragItemSlot == null) return;

		if (dropItemSlot.CanAddStack(dragItemSlot.Item))
		{
			AddStacks(dropItemSlot);
		}
		else if (dropItemSlot.CanReceiveItem(dragItemSlot.Item) && dragItemSlot.CanReceiveItem(dropItemSlot.Item))
		{
			SwapItems(dropItemSlot);
		}
	}

	private void AddStacks(BaseItemSlot dropItemSlot)
	{
		int numAddableStacks = dropItemSlot.Item.MaximumStacks - dropItemSlot.Amount;
		int stacksToAdd = Mathf.Min(numAddableStacks, dragItemSlot.Amount);

		dropItemSlot.Amount += stacksToAdd;
		dragItemSlot.Amount -= stacksToAdd;
	}

	private void SwapItems(BaseItemSlot dropItemSlot)
	{
		EquippableItem dragEquipItem = dragItemSlot.Item as EquippableItem;
		EquippableItem dropEquipItem = dropItemSlot.Item as EquippableItem;

		if (dropItemSlot is EquipmentSlot)
		{
			if (dragEquipItem != null) dragEquipItem.Equip(this);
			if (dropEquipItem != null) dropEquipItem.Unequip(this);
		}
		if (dragItemSlot is EquipmentSlot)
		{
			if (dragEquipItem != null) dragEquipItem.Unequip(this);
			if (dropEquipItem != null) dropEquipItem.Equip(this);
		}
		statPanel.UpdateStatValues();

		Item draggedItem = dragItemSlot.Item;
		int draggedItemAmount = dragItemSlot.Amount;

		dragItemSlot.Item = dropItemSlot.Item;
		dragItemSlot.Amount = dropItemSlot.Amount;

		dropItemSlot.Item = draggedItem;
		dropItemSlot.Amount = draggedItemAmount;
	}

	private void DropItemOutsideUI()
	{
		if (dragItemSlot == null) return;
        BaseItemSlot slot = dragItemSlot;
        TransferToItemContainer(slot);
        DestroyItemInSlot(slot);
        /*
		reallyDropItemDialog.Show();
		BaseItemSlot slot = dragItemSlot;
		reallyDropItemDialog.OnYesEvent += () => DestroyItemInSlot(slot);
        */
    }

	private void DestroyItemInSlot(BaseItemSlot itemSlot)
	{
		itemSlot.Item.Destroy();
		itemSlot.Item = null;
	}

	public void Equip(EquippableItem item)
	{
		if (Inventory.RemoveItem(item))
		{
			EquippableItem previousItem;
			if (EquipmentPanel.AddItem(item, out previousItem))
			{
				if (previousItem != null)
				{
					Inventory.AddItem(previousItem);
					previousItem.Unequip(this);
					statPanel.UpdateStatValues();
				}
				item.Equip(this);
				statPanel.UpdateStatValues();
			}
			else
			{
				Inventory.AddItem(item);
			}
		}
	}

	public void Unequip(EquippableItem item)
	{
		if (Inventory.CanAddItem(item) && EquipmentPanel.RemoveItem(item))
		{
			item.Unequip(this);
			statPanel.UpdateStatValues();
			Inventory.AddItem(item);
		}
	}

	private ItemContainer openItemContainer;

	private void TransferToItemContainer(BaseItemSlot itemSlot)
	{
		Item item = itemSlot.Item;
		if (item != null && openItemContainer.CanAddItem(item))
		{
			Inventory.RemoveItem(item);
			openItemContainer.AddItem(item);
		}
	}

	private void TransferToInventory(BaseItemSlot itemSlot)
	{
		Item item = itemSlot.Item;
		if (item != null && Inventory.CanAddItem(item))
		{
			openItemContainer.RemoveItem(item);
			Inventory.AddItem(item);
		}
	}

	public void OpenItemContainer(ItemContainer itemContainer)
	{
		openItemContainer = itemContainer;

		//Inventory.OnRightClickEvent -= InventoryRightClick;
		//Inventory.OnRightClickEvent += TransferToItemContainer;

		itemContainer.OnRightClickEvent += TransferToInventory;

		itemContainer.OnPointerEnterEvent += ShowTooltip;
		itemContainer.OnPointerExitEvent += HideTooltip;
		itemContainer.OnBeginDragEvent += BeginDrag;
		itemContainer.OnEndDragEvent += EndDrag;
		itemContainer.OnDragEvent += Drag;
		itemContainer.OnDropEvent += Drop;
	}

	public void CloseItemContainer(ItemContainer itemContainer)
	{
		openItemContainer = null;

		//Inventory.OnRightClickEvent += InventoryRightClick;
		//Inventory.OnRightClickEvent -= TransferToItemContainer;

		itemContainer.OnRightClickEvent -= TransferToInventory;

		itemContainer.OnPointerEnterEvent -= ShowTooltip;
		itemContainer.OnPointerExitEvent -= HideTooltip;
		itemContainer.OnBeginDragEvent -= BeginDrag;
		itemContainer.OnEndDragEvent -= EndDrag;
		itemContainer.OnDragEvent -= Drag;
		itemContainer.OnDropEvent -= Drop;
	}

	public void UpdateStatValues()
	{
		statPanel.UpdateStatValues();
	}

    
    public LootBag GetClosestBag()
    {
        lootPickUp.LootBagsNearBy.RemoveAll(LootBag => LootBag == null);
        LootBag closestBag = null;
        float distance = Mathf.Infinity;
        foreach(LootBag lootBag in lootPickUp.LootBagsNearBy)
        {
            
            Vector3 distanceCheck = lootBag.transform.position - GameManager.PlayerInstance.transform.position;
            float currentDistance = distanceCheck.sqrMagnitude;

            if (currentDistance < distance)
            {
                closestBag = lootBag;

                distance = currentDistance;

            }
        }
        if (closestBag != null)
            return closestBag;
        else
            return null;
        
    }


}
