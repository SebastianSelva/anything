﻿using UnityEngine;

public class InventoryInput : MonoBehaviour
{
	[SerializeField] GameObject Inventory;
	[SerializeField] GameObject QuestPanel;
    [SerializeField] GameObject MenuPanel;
    public GameObject OptionPanel;
    [SerializeField] KeyCode[] ToggleInventoryKey;
	[SerializeField] KeyCode[] ToggleQuestKey;
    [SerializeField] KeyCode[] ToggleMenuKey;
    [SerializeField] bool showAndHideMouse = true;
    public bool canInput = true;
    private PlayerController playerController;

    void Start()
    {
        playerController = GameManager.PlayerInstance.GetComponent<PlayerController>();
    }
	void Update()
	{
		ToggleQuest();
		ToggleInventory();
        ToggleMenu();
        Option();
    }

	private void ToggleInventory()
	{
		for (int i = 0; i < ToggleInventoryKey.Length; i++)
		{
			if (Input.GetKeyDown(ToggleInventoryKey[i]) && canInput == true)
			{
                if (MenuPanel.activeSelf == false)
                {
                    if (Inventory.activeSelf == false)
                    {
                        if (QuestPanel.activeSelf == true)
                        {
                            QuestPanel.SetActive(false);
                            Inventory.SetActive(true);
                            playerController.canDash = false;
                        }
                        else
                        {
                            playerController.canDash = false;
                            Inventory.SetActive(true);
                        }
                    }
                    else
                    {
                        playerController.canDash = true;
                        Inventory.SetActive(false);
                    }
                }
                break;
			}
		}
	}

	private void ToggleQuest()
	{
		for (int i = 0; i < ToggleQuestKey.Length; i++)
		{
			if (Input.GetKeyDown(ToggleQuestKey[i]) && canInput == true)
			{
                if (MenuPanel.activeSelf == false)
                {
                    if (QuestPanel.activeSelf == false)
                    {
                        if (Inventory.activeSelf == true)
                        {
                            Inventory.SetActive(false);
                            QuestPanel.SetActive(true);
                            playerController.canDash = false;
                        }
                        else
                        {
                            playerController.canDash = false;
                            QuestPanel.SetActive(true);
                        }
                    }
                    else
                    {
                        playerController.canDash = true;
                        QuestPanel.SetActive(false);
                    }
                }
				break;
			}
		}
	}

    private void ToggleMenu()
    {
        for (int i = 0; i < ToggleMenuKey.Length; i++)
        {
            if (Input.GetKeyDown(ToggleMenuKey[i]))
            {
                if (MenuPanel.activeSelf == false)
                {
                    if (Inventory.activeSelf == true || QuestPanel.activeSelf == true)
                    {
                        Inventory.SetActive(false);
                        QuestPanel.SetActive(false);
                        MenuPanel.SetActive(true);
                        playerController.canDash = false;
                    }
                    else
                    {
                        playerController.canDash = false;
                        Time.timeScale = 0;
                        MenuPanel.SetActive(true);
                    }
                }
                else
                {
                    playerController.canDash = true;
                    Time.timeScale = 1;
                    MenuPanel.SetActive(false);
                }
                break;
            }
        }
    }

    public void ShowMouseCursor()
	{
		if (showAndHideMouse)
		{
			Cursor.visible = true;
			Cursor.lockState = CursorLockMode.None;
		}
	}

	public void HideMouseCursor()
	{
		if (showAndHideMouse)
		{
			Cursor.visible = false;
			Cursor.lockState = CursorLockMode.Locked;
		}
	}

	public void ToggleEquipmentPanel()
	{
		
	}

    void Option()
    {
        if (MenuPanel.activeSelf == false)
            OptionPanel.SetActive(false);
    }

    public void ResumeButton()
    {
        if (MenuPanel.activeSelf == false)
        {
            if (Inventory.activeSelf == true || QuestPanel.activeSelf == true)
            {
                Inventory.SetActive(false);
                QuestPanel.SetActive(false);
                MenuPanel.SetActive(true);
                playerController.canDash = false;
            }
            else
            {
                playerController.canDash = false;
                Time.timeScale = 0;
                MenuPanel.SetActive(true);
            }
        }
        else
        {
            playerController.canDash = true;
            Time.timeScale = 1;
            MenuPanel.SetActive(false);
        }
        
    }


    public void OptionButton()
    {
        if (OptionPanel.activeSelf == true)
            OptionPanel.SetActive(false);
        else
            OptionPanel.SetActive(true);
    }
}
