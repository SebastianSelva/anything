﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class LootViewer : ItemContainer
{
    
    [SerializeField] Transform itemsParent;
    [SerializeField] private Character character;
    LootBag lootBagOpened;
    


    void Start()
    {
        character.OpenItemContainer(this);
    }
    void FixedUpdate()
    {
        
        
        

    }

    protected override void OnValidate()
    {
        if (itemsParent != null)
            itemsParent.GetComponentsInChildren(includeInactive: true, result: ItemSlots);
    }

    void DisplayLoot()
    {
        LootBag lootBag;

        if (character.GetClosestBag() != null)
            lootBag = character.GetClosestBag();
        else
            return;


        lootBagOpened = lootBag;

        if (lootBag != null)
        {
            for (int i = 0; i < lootBagOpened.Items.Count; i++)
            {
                AddItem(lootBagOpened.Items[i]);
                Debug.Log(lootBagOpened.Items[i]);
            }
        }
        else
        {
            Clear();
        }
    }

    void OnEnable()
    {
        
        DisplayLoot();
    }

    void OnDisable()
    {
        if (lootBagOpened != null)
        {
            transferLoot();
        }
        else
        {
            NewBag();
        }

        SetDefault();
        lootBagOpened = null;
        
    }

    void transferLoot()
    {
        lootBagOpened.ClearItems();
            for (int i = 0; i < this.ItemSlots.Count; i++)
            {
                if (this.ItemSlots[i].Item != null)
                    lootBagOpened.AddItem(this.ItemSlots[i].Item);
                
                
            }
        
    }

    public void SetDefault()
    {
        for (int i = 0; i < ItemSlots.Count; i++)
        {
            ItemSlots[i].Item = null;
        }
    }

    public void NewBag()
    {
        GameObject Drop = Instantiate(GameManager.GM.LootInstance, new Vector3(GameManager.PlayerInstance.transform.position.x, 0, GameManager.PlayerInstance.transform.position.z), Quaternion.identity);

        AddStacksToDrop(Drop);
    }

    public void AddStacksToDrop(GameObject drop)
    {

        for (int i = 0; i < this.ItemSlots.Count; i++)
        {
            if (this.ItemSlots[i].Item != null)
            {
                
                if(ItemSlots[i].Amount == 1)
                    drop.GetComponent<LootBag>().Items.Add(ItemSlots[i].Item);
                else if(ItemSlots[i].Amount > 1)
                {
                    for (int l = 0; l < ItemSlots[i].Amount; l++)
                    {
                        drop.GetComponent<LootBag>().Items.Add(ItemSlots[i].Item);
                    }
                }

            }


        }


        
    }
  
 }
