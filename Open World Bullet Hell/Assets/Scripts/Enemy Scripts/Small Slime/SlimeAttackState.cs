﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FSM;

public class SlimeAttackState : State<AI> 
{
    private static SlimeAttackState _Instance;
    private SlimeAttackState()
    {
        if (_Instance != null)
        {
            return;
        }

        _Instance = this;
    }

    public static SlimeAttackState Instance
    {
        get
        {
            if (_Instance == null)
            {
                new SlimeAttackState();
            }

            return _Instance;
        }
    }
    public override void EnterState(AI _owner)
    {
        _owner.StartCoroutine(_owner.PluleMovementPlayer());
    }

    public override void ExiteState(AI _owner)
    {
        _owner.StopCoroutine(_owner.PluleMovementPlayer());
    }

    public override void UpdateState(AI _owner)
    {

        _owner.StartCoroutine(_owner.PluleMovementPlayer());
        _owner.shoot();

        if (_owner.state == 0)
        {

            _owner.stateMachine.ChangeState(SlimePatrol.Instance);
        }

    }

    
}
