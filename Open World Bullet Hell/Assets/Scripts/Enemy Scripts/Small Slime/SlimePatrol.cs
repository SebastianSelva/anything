﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FSM;
public class SlimePatrol : State<AI>
{

    private static SlimePatrol _Instance;

    private SlimePatrol()
    {
        if (_Instance != null)
        {
            return;
        }

        _Instance = this;
    }

    public static SlimePatrol Instance
    {
        get
        {
            if (_Instance == null)
            {
                new SlimePatrol();
            }

            return _Instance;
        }
    }

    public override void EnterState(AI _owner)
    {
        
    }

    public override void ExiteState(AI _owner)
    {
        
    }

    public override void UpdateState(AI _owner)
    {

        _owner.Parol();

        if (_owner.state == 1)
        {
            
            _owner.stateMachine.ChangeState(SlimeAttackState.Instance);
        }

    }
}
