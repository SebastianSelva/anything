﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FSM;

public class OrcAI : MonoBehaviour {

    private enum EnemyState { Patrol, Attack }
    public int state;
    public float playerDistance;
    public float speed = 6;
    public float rotateSpeed;
    public float distFromCamp;
    public float MinDist;
    public float MaxDist;
    public Sprite shotSprite;
    public float shotSpeed;
    public GameObject Shot;
    public float OrcAttackSpeed;
    public float attackTime;

    public static GameObject Target;
    public float angle;

    public static void GetTarget()
    {
        if (Target == null)
            Target = GameObject.FindGameObjectWithTag("Player");

        return;
    }


    public StateMachine<OrcAI> stateMachine { get; set; }

    public void Start()
    {

        stateMachine = new StateMachine<OrcAI>(this);
        stateMachine.ChangeState(OrcPatrolState.Instance);
        GetTarget();


    }

    public void Update()
    {
        

        playerDistance = Vector3.Distance(this.transform.position, Target.transform.position);

        if (state == 0 && playerDistance <= 8)
        {
            state = (int)EnemyState.Attack;
        }

        if (state == 1 && playerDistance >= 15)
        {
            state = (int)EnemyState.Patrol;
        }




        stateMachine.Update();

        attackTime -= Time.deltaTime;
    }

    public void MoveToPlayer()
    {
        transform.LookAt(GameManager.PlayerInstance.transform);

        

        if (Vector3.Distance(transform.position, GameManager.PlayerInstance.transform.position) >= MinDist)
        {
            transform.position += transform.forward * speed * Time.deltaTime;
        }
    }

    public void MoveToCamp()
    {
        transform.LookAt(campFire.transform);



        if (Vector3.Distance(transform.position, campFire.transform.position) >= 2.9)
        {
            transform.position += transform.forward * speed * Time.deltaTime;
        }
    }


    public void OrcShoot()
    {
        if (attackTime <= 0)
        {


            OneNormalShot(180);
            attackTime = OrcAttackSpeed;
        }


    }


    void OneNormalShot(float offset)
    {
        GameObject shoot = Instantiate(Shot, transform.position, transform.rotation * Quaternion.Euler(0f, offset, 0f));
        shoot.GetComponent<SpriteRenderer>().sprite = shotSprite;
        shoot.GetComponent<EnemyShot>().owner = this.gameObject;
        shoot.transform.Rotate(new Vector3(-90, 0, -90));
        shoot.GetComponent<Rigidbody>().velocity = (GameManager.PlayerInstance.transform.position - transform.position).normalized * shotSpeed;

    }

    void instatiateShot(float offset) //offset 180 for straight shot
    {
        GameObject shoot = Instantiate(Shot, transform.position, transform.rotation * Quaternion.Euler(0f, offset, 0f));
        shoot.GetComponent<SpriteRenderer>().sprite = shotSprite;
        shoot.GetComponent<EnemyShot>().owner = this.gameObject;
        shoot.transform.Rotate(new Vector3(-90, 0, -90));
        shoot.GetComponent<Rigidbody>().velocity = (GameManager.PlayerInstance.transform.position - transform.position).normalized * shotSpeed;

    }




    void CircleShot(int numOfProjectiles)
    {
        float angleStep = 360f / numOfProjectiles;

        float angle = 0f;
        Vector3 startpoint = transform.position;

        for (int i = 0; i <= numOfProjectiles - 1; i++)
        {
            //Direction Calculation
            float projectileDirXPosition = startpoint.x + Mathf.Sin((angle * Mathf.PI) / 180) * 1f;
            float projectileDirYPosition = startpoint.y + Mathf.Cos((angle * Mathf.PI) / 180) * 1f;

            Vector3 ProjectileVector = new Vector3(projectileDirXPosition, projectileDirYPosition, 0);
            Vector3 ProjectileMoveDirection = (ProjectileVector - startpoint).normalized * 20;

            GameObject shot = Instantiate(Shot, startpoint, Quaternion.identity);
            shot.GetComponent<SpriteRenderer>().sprite = shotSprite;
            shot.GetComponent<EnemyShot>().owner = this.gameObject;
            shot.GetComponent<Rigidbody>().velocity = new Vector3(ProjectileMoveDirection.x, 0, ProjectileMoveDirection.y);
            shot.transform.rotation = Quaternion.Euler(-90, angle, 90);
            angle += angleStep;
        }
    }


    void BurstShot(int numOfProjectiles)
    {
        float angleStep = 180f / numOfProjectiles;
        Vector3 pos = GameManager.PlayerInstance.transform.position - transform.position;
        float angle = (Mathf.Atan2(pos.x, pos.z) * Mathf.Rad2Deg) - 90;
        Debug.Log(angle);
        Vector3 startpoint = transform.position;

        for (int i = 0; i <= numOfProjectiles - 1; i++)
        {
            //Direction Calculation
            float projectileDirXPosition = startpoint.x + Mathf.Sin((angle * Mathf.PI) / 180) * 1f;
            float projectileDirYPosition = startpoint.y + Mathf.Cos((angle * Mathf.PI) / 180) * 1f;

            Vector3 ProjectileVector = new Vector3(projectileDirXPosition, projectileDirYPosition, 0);
            Vector3 ProjectileMoveDirection = (ProjectileVector - startpoint).normalized * 20;

            GameObject shot = Instantiate(Shot, startpoint, Quaternion.identity);
            shot.GetComponent<SpriteRenderer>().sprite = shotSprite;
            shot.GetComponent<EnemyShot>().owner = this.gameObject;
            shot.GetComponent<Rigidbody>().velocity = new Vector3(ProjectileMoveDirection.x, 0, ProjectileMoveDirection.y);
            shot.transform.rotation = Quaternion.Euler(-90, angle, 90);
            angle += angleStep;
        }
    }

    public GameObject campFire;
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "CampFire")
        {
            campFire = col.gameObject;
        }
    }

}
