﻿using FSM;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrcPatrolState : State<OrcAI>
{
    private static OrcPatrolState _Instance;

    private OrcPatrolState()
    {
        if (_Instance != null)
        {
            return;
        }

        _Instance = this;
    }

    public static OrcPatrolState Instance
    {
        get
        {
            if (_Instance == null)
            {
                new OrcPatrolState();
            }

            return _Instance;
        }
    }

    public override void EnterState(OrcAI _owner)
    {
        Debug.Log("Entering Patrol State");
    }

    public override void ExiteState(OrcAI _owner)
    {
        Debug.Log("Exiting Patrol State");
    }

    public override void UpdateState(OrcAI _owner)
    {



        if (_owner.state == 1)
        {
            _owner.stateMachine.ChangeState(OrcAttackState.Instance);
        }
    }
}
