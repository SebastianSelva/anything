﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FSM;

public class OrcAttackState : State<OrcAI>
    {

    private static OrcAttackState _Instance;

    private OrcAttackState()
    {
        if (_Instance != null)
        {
            return;
        }

        _Instance = this;
    }

    public static OrcAttackState Instance
    {
        get
        {
            if (_Instance == null)
            {
                new OrcAttackState();
            }

            return _Instance;
        }
    }

    public override void EnterState(OrcAI _owner)
    {
        Debug.Log("Entering Attack State");
        _owner.rotateSpeed = _owner.rotateSpeed * (Random.Range(0.0F, 1.0F) < 0.5F ? 1 : -1);
    }

    public override void ExiteState(OrcAI _owner)
    {
        Debug.Log("Exiting Attack State");
    }

    public override void UpdateState(OrcAI _owner)
    {
        if(_owner.campFire != null)
        {
            _owner.distFromCamp = Vector3.Distance(_owner.gameObject.transform.position, _owner.campFire.transform.position);
            if( _owner.distFromCamp > 3)
            {
                _owner.MoveToCamp();
            }
            else
            {
                _owner.gameObject.transform.RotateAround(_owner.campFire.transform.position, Vector3.up, _owner.rotateSpeed * Time.deltaTime);
            }
            
        }
        else
        {
            _owner.MoveToPlayer();
        }

        _owner.OrcShoot();

        if (_owner.state == 0)
        {
            _owner.stateMachine.ChangeState(OrcPatrolState.Instance);
        }
    }
}
