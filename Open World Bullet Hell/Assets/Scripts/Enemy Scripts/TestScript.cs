﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour {

    public GameObject Shot;
    public float attackTime;
    public float attackSpeed;
    void Update()
    {
        shoot();
        attackTime -= Time.deltaTime;
    }

    void instatiateShot(float offset)
    {
        GameObject shoot = Instantiate(Shot, transform.position, transform.rotation * Quaternion.Euler(0f, offset, 0f));

        shoot.transform.Rotate(new Vector3(-90, 0, -90));
        

    }

    public void shoot()
    {
        if( attackTime<= 0)
        {
            instatiateShot(180);
            attackTime = attackSpeed;
        }
    }
}
