﻿using UnityEngine;
using FSM;

public class GoblinPatrolState : State<GoblinAI>
{
    private static GoblinPatrolState _Instance;

    private GoblinPatrolState()
    {
        if (_Instance != null)
        {
            return;
        }

        _Instance = this;
    }

    public static GoblinPatrolState Instance
    {
        get
        {
            if(_Instance == null)
            {
                new GoblinPatrolState();
            }

            return _Instance;
        }
    }

    public override void EnterState(GoblinAI _owner)
    {
        //Debug.Log("Entering Patrol State");
        _owner.speed = 2;
    }

    public override void ExiteState(GoblinAI _owner)
    {
       // Debug.Log("Exiting Patrol State");
    }

    public override void UpdateState(GoblinAI _owner)
    {

        _owner.Parol();

        if (_owner.state == 1)
        {
            _owner.speed = 5;
            _owner.stateMachine.ChangeState(GoblinAttackState.Instance);
        }
    }
}
