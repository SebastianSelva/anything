﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FSM;

public class GoblinAI : MonoBehaviour
{
    private enum EnemyState { Patrol , Attack }
    public int state;
    public bool switchState = false;
    public float playerDistance;
    public float speed = 5;
    
    public float MinDist;
    public float MaxDist;

    public float shotSpeed;
    public GameObject Shot;
    public float GoblinAttackSpeed;
    public float attackTime;
    public static GameObject Target;

    public Sprite shotSprite;
    public static void GetTarget()
    {
        if (Target == null)
            Target = GameObject.FindGameObjectWithTag("Player");

        return;
    }
    

    public StateMachine<GoblinAI> stateMachine { get; set; }

    public void Start()
    {
        StartingPoint = transform.position;
        stateMachine = new StateMachine<GoblinAI>(this);
        stateMachine.ChangeState(GoblinPatrolState.Instance);
        GetTarget();

        
    }

    public void Update()
    {
        

        playerDistance = Vector3.Distance(this.transform.position, Target.transform.position);

        if (state == 0 && playerDistance <= 5)
        {
            state = (int)EnemyState.Attack;
        }
        
        if (state == 1 && playerDistance >= 15)
        {
            state = (int)EnemyState.Patrol;
        }


        

        stateMachine.Update();

        attackTime -= Time.deltaTime;
    }

    public void MoveToPlayer()
    {
        transform.LookAt(GameManager.PlayerInstance.transform);
 
            GoblinShoot();

        if (Vector3.Distance(transform.position, GameManager.PlayerInstance.transform.position) >= MinDist)
        {
            transform.position += transform.forward * speed * Time.deltaTime;
        }
    }


    void GoblinShoot()
    {
        if (attackTime <= 0 )
        {
            OneNormalShot(180); //180 offset is straight
            attackTime = GoblinAttackSpeed;
        }

        
    }


    void OneNormalShot(float offset)
    {
        GameObject shoot = Instantiate(Shot, transform.position, transform.rotation * Quaternion.Euler(0f, offset, 0f));
        shoot.GetComponent<EnemyShot>().owner = this.gameObject;
        shoot.transform.Rotate(new Vector3(-90, 0, - 90));
        shoot.GetComponent<Rigidbody>().velocity = (GameManager.PlayerInstance.transform.position - transform.position).normalized * shotSpeed;
        shoot.GetComponent<SpriteRenderer>().sprite = shotSprite;
    }


    

    void CircleShot(int numOfProjectiles)
    {
        float angleStep = 360f / numOfProjectiles;
        
        float angle = 0f;
        Vector3 startpoint = transform.position;

        for (int i = 0; i <= numOfProjectiles - 1; i++)
        {
            //Direction Calculation
            float projectileDirXPosition = startpoint.x + Mathf.Sin((angle * Mathf.PI) / 180) * 1f;
            float projectileDirYPosition = startpoint.y + Mathf.Cos((angle * Mathf.PI) / 180) * 1f;

            Vector3 ProjectileVector = new Vector3(projectileDirXPosition, projectileDirYPosition, 0);
            Vector3 ProjectileMoveDirection = (ProjectileVector - startpoint).normalized * 20;

            GameObject shot = Instantiate(Shot, startpoint, Quaternion.identity);
            shot.GetComponent<EnemyShot>().owner = this.gameObject;
            shot.GetComponent<Rigidbody>().velocity = new Vector3(ProjectileMoveDirection.x, 0, ProjectileMoveDirection.y);
            shot.transform.rotation =  Quaternion.Euler(-90, angle, 90);
            angle += angleStep;
        }
    }


    void BurstShot(int numOfProjectiles)
    {
        float angleStep = 180f / numOfProjectiles;
        Vector3 pos = GameManager.PlayerInstance.transform.position - transform.position;
        float angle = (Mathf.Atan2(pos.x, pos.z) * Mathf.Rad2Deg) - 90;
        Debug.Log(angle);
        Vector3 startpoint = transform.position;

        for (int i = 0; i <= numOfProjectiles - 1; i++)
        {
            //Direction Calculation
            float projectileDirXPosition = startpoint.x + Mathf.Sin((angle * Mathf.PI) / 180) * 1f;
            float projectileDirYPosition = startpoint.y + Mathf.Cos((angle * Mathf.PI) / 180) * 1f;

            Vector3 ProjectileVector = new Vector3(projectileDirXPosition, projectileDirYPosition, 0);
            Vector3 ProjectileMoveDirection = (ProjectileVector - startpoint).normalized * 20;

            GameObject shot = Instantiate(Shot, startpoint, Quaternion.identity);
            shot.GetComponent<EnemyShot>().owner = this.gameObject;
            shot.GetComponent<Rigidbody>().velocity = new Vector3(ProjectileMoveDirection.x, 0, ProjectileMoveDirection.y);
            shot.transform.rotation = Quaternion.Euler(-90, angle, 90);
            angle += angleStep;
        }
    }

    public float timeTilNextMove;
    public Vector3 StartingPoint , nextPos;
    public float range = 1;

    public void Parol()
    {
        if (timeTilNextMove >= 0 )
        {

            transform.position = Vector3.MoveTowards(  transform.position , new Vector3 (nextPos.x + StartingPoint.x , transform.localPosition.y ,nextPos.y + StartingPoint.z) , speed * Time.deltaTime);
           
            timeTilNextMove -= Time.deltaTime;
        }
        else
        {
       
            nextPos =  (Random.insideUnitCircle * range);

            //Debug.Log(nextPos + name);
            timeTilNextMove = Random.Range(1f, 1.5f);
            
        }
    }



}
