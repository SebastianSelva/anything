﻿using UnityEngine;
using FSM;

public class GoblinAttackState : State<GoblinAI>
{
    private static GoblinAttackState _Instance;

    private GoblinAttackState()
    {
        if (_Instance != null)
        {
            return;
        }

        _Instance = this;
    }

    public static GoblinAttackState Instance
    {
        get
        {
            if (_Instance == null)
            {
                new GoblinAttackState();
            }

            return _Instance;
        }
    }

    public override void EnterState(GoblinAI _owner)
    {
        Debug.Log("Entering Attack State");
    }

    public override void ExiteState(GoblinAI _owner)
    {
        Debug.Log("Exiting Attack State");
    }

    public override void UpdateState(GoblinAI _owner)
    {
        _owner.MoveToPlayer();

        if (_owner.state == 0)
        {
            _owner.stateMachine.ChangeState(GoblinPatrolState.Instance);
        }
    }
}