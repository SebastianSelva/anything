﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum Enemy
{
    slime,
    goblin,
    orc,
    Necro,
    none,
}
public class EnemyStats : MonoBehaviour {
    [SerializeField]
    public int type;
    private float EnemyHealth;
    public float maxHealth = 100;
    public float EnemyAttack;
    public float WeaponDamageMax;

    public float WeaponDamageMin;

    private HealthBar HealthBar;


    public void resetHealth()
    {
        EnemyHealth = maxHealth;
    }
    void Awake()
    {
        EnemyHealth = maxHealth;
        EnemyAttack = 3;
        HealthBar = gameObject.GetComponentInChildren<HealthBar>();
        WeaponDamageMin = 10;
        WeaponDamageMax = 20;
    }
	void Update ()
    {
        

		if (EnemyHealth <= 0)
        {
            gameObject.GetComponent<LootTable>().DropItem();
            counter();
            AwardXp();
            Destroy(gameObject);
        }
	}

    void AwardXp()
    {
        float baseXp = maxHealth / 10;
        float XpCap = maxHealth * 0.1f;
        if(baseXp >= XpCap)
        {
            GameManager.PlayerInstance.GetComponent<Character>().Xp += (int)XpCap;
        }else
        {
            GameManager.PlayerInstance.GetComponent<Character>().Xp += (int)baseXp;
        }

    }

    public void DealDamage(float _WeaponDamageMin , float _WeaponDamageMax , float _AttackStat)
    {
        float damageMultiplier = 0.5f + _AttackStat / 50;
        float damage = (Random.Range(_WeaponDamageMin, _WeaponDamageMax) * damageMultiplier);
        EnemyHealth -= Mathf.Round(damage);
        HealthBar.SetHealthBar(EnemyHealth / maxHealth);
        Debug.Log(Mathf.Round(damage) + " damage dealt");
        if(gameObject.GetComponent<AI>())
        if (gameObject.GetComponent<AI>().state == 0)
            gameObject.GetComponent<AI>().state = 1;
    }

    void counter()
    {
        if (GameManager.gameInfo.IsOnSlimeQuest == true && type == 0)
        {
            GameManager.gameInfo.SlimeQuestKillCount++;
            Debug.Log(GameManager.gameInfo.SlimeQuestKillCount);
        }
        else if (GameManager.gameInfo.IsOnGoblinQuest == true && type == 1)
        {
            GameManager.gameInfo.GoblinQuestKillCount++;
            Debug.Log(GameManager.gameInfo.GoblinQuestKillCount);
        }
        else if (GameManager.gameInfo.IsOnOrcQuest == true && type == 2)
        {
            GameManager.gameInfo.OrcQuestKillCount++;
            Debug.Log(GameManager.gameInfo.OrcQuestKillCount);
        }
        else if (GameManager.questManager.IsOnSlimeKingQuest == true && type == 3)
        {
            GameManager.gameInfo.NecroKillCount++;
            Debug.Log(GameManager.gameInfo.NecroKillCount);
        }
    }


}
