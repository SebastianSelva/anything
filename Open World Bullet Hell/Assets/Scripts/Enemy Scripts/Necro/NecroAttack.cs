﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FSM;

public class NecroAttack : State<NecroAI>
{
    private static NecroAttack _Instance;
    private NecroAttack()
    {
        if (_Instance != null)
        {
            return;
        }

        _Instance = this;
    }

    public static NecroAttack Instance
    {
        get
        {
            if (_Instance == null)
            {
                new NecroAttack();
            }

            return _Instance;
        }
    }
    public override void EnterState(NecroAI _owner)
    {
        _owner.StartCoroutine(_owner.PluleMovementPlayer());
    }

    public override void ExiteState(NecroAI _owner)
    {
        _owner.StopCoroutine(_owner.PluleMovementPlayer());
    }

    public override void UpdateState(NecroAI _owner)
    {

        _owner.StartCoroutine(_owner.PluleMovementPlayer());
        _owner.shoot();

        if (_owner.state == 0)
        {

            _owner.stateMachine.ChangeState(NecroPatrol.Instance);
        }

    }


}
