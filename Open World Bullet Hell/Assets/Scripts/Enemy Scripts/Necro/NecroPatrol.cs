﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FSM;
public class NecroPatrol : State<NecroAI>
{

    private static NecroPatrol _Instance;

    private NecroPatrol()
    {
        if (_Instance != null)
        {
            return;
        }

        _Instance = this;
    }

    public static NecroPatrol Instance
    {
        get
        {
            if (_Instance == null)
            {
                new NecroPatrol();
            }

            return _Instance;
        }
    }

    public override void EnterState(NecroAI _owner)
    {

    }

    public override void ExiteState(NecroAI _owner)
    {

    }

    public override void UpdateState(NecroAI _owner)
    {

        _owner.Parol();

        if (_owner.state == 1)
        {

            _owner.stateMachine.ChangeState(NecroAttack.Instance);
        }

    }
}
