﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShot : MonoBehaviour
{

    public GameObject owner;
    public int speed;
    public Vector3 velocity = new Vector3(0.0f, 0.0f, 0.0f);
    public GameObject Enemy;
    private Vector3 currnt;

    

    void Awake()
    {
        Destroy(this.gameObject, 2);
        transform.Rotate(0, 0, 0);
    }

    void Update()
    {
        Vector3 currentPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        Vector3 newPostion = currentPosition + velocity  * speed * Time.deltaTime;

        gameObject.transform.position = newPostion;
        currnt = newPostion;
        
        Collider[] hits = Physics.OverlapSphere(currentPosition, 0.45f);
                     

        foreach (Collider hit in hits)
        {
            GameObject other = hit.gameObject;
            if (other != Enemy)
            {
                if (other.CompareTag("Player"))
                {

                    //Deal Damage To Player
                    if (owner != null)
                    GameManager.PlayerInstance.GetComponent<PlayerController>().PlayerDamage(owner.GetComponent<EnemyStats>().WeaponDamageMin, owner.GetComponent<EnemyStats>().WeaponDamageMax, owner.GetComponent<EnemyStats>().EnemyAttack);
                    Destroy(gameObject);
                }
            }
        }


    }

    public void OnDrawGizmosSelected()
    {
        Gizmos.DrawSphere(currnt, .45f);
    }
}
