﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour {
    

    public static GameManager GM;
    public static UI_Manager UI;
    public static GameObject PlayerInstance;
    public static GameInfo gameInfo;
    public static SaveManager SaveManager;
    public static QuestManager questManager;
    public static ItemSaveManager itemSave;
    public GameObject OptionPanel;
    private float Load_PlayerHealth;
    public static bool isDead;
    public GameObject HelpPanel;
    public GameObject talkToKing;
    public GameObject LootInstance;


    public Item[] startingItems;

    public GameObject King;
    public GameObject Steward;


    void Awake()
    {

        

        GM = gameObject.GetComponent<GameManager>();
  
        isDead = false;

        if (PlayerInstance == null)
            PlayerInstance = GameObject.FindGameObjectWithTag("Player");

        if (UI == null)
            UI = gameObject.GetComponent<UI_Manager>();

        if (SaveManager == null)
            SaveManager = gameObject.GetComponent<SaveManager>();

        if (questManager == null)
            questManager = gameObject.GetComponent<QuestManager>();

        if (itemSave == null)
            itemSave = gameObject.GetComponent<ItemSaveManager>();

        if (gameInfo == null)
            gameInfo = new GameInfo();

        gameInfo.SetNPC();

        if (Overseer.NewGame == true)
        {
            Debug.Log("New Game");
            PlayerInstance.GetComponent<Character>().Inventory.Clear();
            PlayerInstance.GetComponent<Character>().EquipmentPanel.RemoveAllItems();

            
            PlayerInstance.GetComponent<Character>().Inventory.AddItem(startingItems[0]);
            PlayerInstance.GetComponent<Character>().Inventory.AddItem(startingItems[1]);
            PlayerInstance.GetComponent<Character>().Inventory.AddItem(startingItems[2]);
            HelpPanel.SetActive(true);
            talkToKing.SetActive(true);
        }

        if (Overseer.NewGame == false)
        {
            SaveManager.LoadGame();
            itemSave.LoadEquipment(PlayerInstance.GetComponent<Character>());
            itemSave.LoadInventory(PlayerInstance.GetComponent<Character>());

            Debug.Log("Game Loaded");
        }
    }

    void Start()
    {
        
    }
    void Update()
    {
        

        
    }

    
    void OnApplicationQuit()
    {
        itemSave.SaveEquipment(PlayerInstance.GetComponent<Character>());
        itemSave.SaveInventory(PlayerInstance.GetComponent<Character>());
        SaveManager.SaveGame();
    }

    
    public void MainMenu()
    {
        itemSave.SaveEquipment(PlayerInstance.GetComponent<Character>());
        itemSave.SaveInventory(PlayerInstance.GetComponent<Character>());
        SaveManager.SaveGame();
        SceneManager.LoadScene(0);
    }
    
   

    public void ExitGame()
    {
        Application.Quit();
    }

    

    
}
