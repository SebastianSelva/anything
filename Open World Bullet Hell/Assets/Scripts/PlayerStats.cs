﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerStats : MonoBehaviour {

    [SerializeField]
    public float PlayerHealth;
    public float maxHealth = 100;
    public float PlayerAttack;

    public float WeaponDamageMax;

    public float WeaponDamageMin;

    public HealthBar healthBar;
    public GameObject Player;
    public PlayerController playerController;

	void Awake()
    {
        Player = this.gameObject;
        playerController = Player.GetComponent<PlayerController>();
        healthBar = GameObject.FindGameObjectWithTag("PlayerHealthBar").GetComponent<HealthBar>();
        PlayerHealth = maxHealth;
        PlayerAttack = 5;
        WeaponDamageMin = 10;
        WeaponDamageMax = 20;
    }
	
	void Update ()
    {
	    if(PlayerHealth <= 0)
        {
            GameManager.isDead = true;
            playerController.canMove = false;
            gameObject.transform.position = Vector3.zero;
        }
	}

    public void DealDamage(float _WeaponDamageMin, float _WeaponDamageMax, float _AttackStat)
    {
        float damage = (Random.Range(_WeaponDamageMin, _WeaponDamageMax) * _AttackStat) / 5;
        PlayerHealth -= Mathf.Round(damage);
        healthBar.SetHealthBar(PlayerHealth / maxHealth);
        Debug.Log(Mathf.Round(damage) + " damage dealt");
    }


    
}
