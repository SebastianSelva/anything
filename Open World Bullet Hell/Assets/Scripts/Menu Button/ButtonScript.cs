﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour
{
    public Button[] buttons; // ordered from first to last : New Game = 0 , Continue = 1 , Option = 2 , Quit = 3
    public GameObject OptionsPanel;
    void Awake()
    {
        buttons = this.GetComponentsInChildren<Button>();
        ContinueButtonAvailable();
    }
	
    public void NewGame()
    {
        Overseer.NewGame = true;
        SceneManager.LoadSceneAsync("Main");
    }

    public void Continue()
    {
        Overseer.NewGame = false;
        SceneManager.LoadSceneAsync("Main");
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void ContinueButtonAvailable()
    {
        if (Directory.Exists(Application.persistentDataPath + "/Save"))
        {
            buttons[1].interactable = true;
        }
        else
        {
            buttons[1].interactable = false;
            buttons[1].GetComponent<Image>().color = buttons[1].colors.disabledColor;
        }
    }

    public void OptionButton()
    {
        if(OptionsPanel.activeSelf == true)
        {
            OptionsPanel.SetActive(false);
        }
        else
        {
            OptionsPanel.SetActive(true);
        }
    }

}
