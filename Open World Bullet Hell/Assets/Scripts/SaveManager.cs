﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveManager : MonoBehaviour 
{
    public ItemDatabase itemDatabase;
    public QuestDataBase questDataBase;
    public GameObject player;
    public GameObject inventory;
    public EquipmentPanel equipmentPanel;
    
    
    void Awake()
    {
        player = GameManager.PlayerInstance;
        
        

    }

    public bool isSaveFile()
    {
        return Directory.Exists(Application.persistentDataPath + "/Save");
    }

    public void NewGame()
    {
        if(File.Exists(Application.persistentDataPath + "/Save"))
        {
            File.Delete(Application.persistentDataPath + "/Save");
        }
    }

    public void SaveGame()
    {
        if(!isSaveFile())
            Directory.CreateDirectory(Application.persistentDataPath + "/Save");


        if (!Directory.Exists(Application.persistentDataPath + "/Save/Character"))
            Directory.CreateDirectory(Application.persistentDataPath + "/Save/Character");
        
        
        SaveCharacter();

        if (!Directory.Exists(Application.persistentDataPath + "/Save/Game"))
            Directory.CreateDirectory(Application.persistentDataPath + "/Save/Game");
        SaveQuests();
        SaveGameInfo();

        Debug.Log("Game Saved");
    }

    public void LoadGame()
    {
        if (isSaveFile())
        {
            LoadCharacter();
            LoadQuests();
            LoadGameInfo();
            
        }
        Debug.Log("Game Loaded");
    }

    void SaveCharacter()
    {
        BinaryFormatter bf = new BinaryFormatter();
        string path = Application.persistentDataPath + "/Save/Character/CharacterSave.dat";
        FileStream file = new FileStream(path , FileMode.Create);
        SaveStats save = new SaveStats();
        save.GetValues();
        bf.Serialize(file, save);
        file.Close();
    }

    void LoadCharacter()
    {
        if(File.Exists(Application.persistentDataPath + "/Save/Character/CharacterSave.dat"))
        {
            string path = Application.persistentDataPath + "/Save/Character/CharacterSave.dat";
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = new FileStream(path , FileMode.Open);
            
            SaveStats save = bf.Deserialize(file) as SaveStats;
            save.SetValues();
            //Debug.Log(save.defence);
            //Debug.Log(save.x);
            //Debug.Log(save.z);
            file.Close();

        }

        
    }

    void SaveGameInfo()
    {
        BinaryFormatter bf = new BinaryFormatter();
        string path = Application.persistentDataPath + "/Save/Game/GameInfo.dat";
        FileStream file = new FileStream(path, FileMode.Create);
        GameInfo gameInfo = new GameInfo();
        GameManager.gameInfo.PrepareForSaveing();
        gameInfo.Saving(GameManager.gameInfo);
        bf.Serialize(file, gameInfo);
        file.Close();
    }

    void LoadGameInfo()
    {
        if (File.Exists(Application.persistentDataPath + "/Save/Game/GameInfo.dat"))
        {
            string path = Application.persistentDataPath + "/Save/Game/GameInfo.dat";
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = new FileStream(path, FileMode.Open);

            GameInfo gameInfo = bf.Deserialize(file) as GameInfo;
            GameManager.gameInfo.Saving(gameInfo);
            GameManager.gameInfo.SetOverrides();
            file.Close();
        }


    }

    void SaveQuests()
    {
        for (int i = 0; i < GameManager.questManager.CurrentQuests.Count; i++)
        {
            BinaryFormatter bf = new BinaryFormatter();

            string path = Application.persistentDataPath + "/Save/Game/Quest" + i + ".dat";
            FileStream file = new FileStream(path, FileMode.Create);
            QuestSaveData quest = new QuestSaveData();
            quest.id = GameManager.questManager.CurrentQuests[i].ID;

            bf.Serialize(file , quest);

            file.Close();
        }
    }

    void LoadQuests()
    {
        
        
            for (int i = 0; i < 8; i++)
            {
                if (File.Exists(Application.persistentDataPath + "/Save/Game/Quest" + i + ".dat"))
                {
                    string path = Application.persistentDataPath + "/Save/Game/Quest" + i + ".dat";
                    BinaryFormatter bf = new BinaryFormatter();
                    FileStream file = new FileStream(path, FileMode.Open);
                    QuestSaveData questSave = bf.Deserialize(file) as QuestSaveData;
                    Quest quest = questDataBase.GetItemCopy(questSave.id);
                    Debug.Log(quest);
                    GameManager.questManager.CurrentQuests.Add(quest);
                    file.Close();
                }

            }
            
        


    }


}
