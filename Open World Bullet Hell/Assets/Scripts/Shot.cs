﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot : MonoBehaviour {

    

    public Vector3 velocity = new Vector3(0.0f, 0.0f, 0.0f);
    public int shotSpeed;
    public float Range;

    void Awake()
    {
        GetComponent<SpriteRenderer>().sprite = GameManager.PlayerInstance.GetComponent<Character>().ShotSprite;
        shotSpeed = GameManager.PlayerInstance.GetComponent<Character>().ShotSpeed;
        Range = GameManager.PlayerInstance.GetComponent<Character>().Range;
        Destroy(this.gameObject, GameManager.PlayerInstance.GetComponent<Character>().Range);
    }

    void Update ()
    {
        transform.position -= transform.right * Time.deltaTime * shotSpeed;
        Vector3 currentPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        Vector3 newPostion = currentPosition + velocity * Time.deltaTime;
        gameObject.transform.position = newPostion;

        Collider[] hits = Physics.OverlapSphere(currentPosition, 0.45f);


        foreach (Collider hit in hits)
        {
            GameObject other = hit.gameObject;
            if(other != GameManager.PlayerInstance)
            {
                if(other.CompareTag("Enemy"))
                {
                    other.GetComponent<EnemyStats>().DealDamage(
                        GameManager.PlayerInstance.GetComponent<Character>().MinimumDamage,
                        GameManager.PlayerInstance.GetComponent<Character>().MaximumDamage,
                        GameManager.PlayerInstance.GetComponent<Character>().Strength.Value);
                    Debug.Log(GameManager.PlayerInstance.GetComponent<Character>().Strength.Value);
                    Destroy(this.gameObject);
                }
            }
        }
        
	}
}
