﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestManager : MonoBehaviour
{

    public List<Quest> CurrentQuests = new List<Quest>();
    public QuestSlot[] questSlots;
    public GameObject QuestParent;
    public QuestDataBase questDataBase;

    public GameObject questPopUp;


    public bool IsOnSlimeQuest;
    public bool IsOnGoblinQuest;
    public bool IsOnOrcQuest;
    public bool IsOnSlimeKingQuest;


    void Awake()
    {
        questSlots = QuestParent.GetComponentsInChildren<QuestSlot>();
    }

    public void AssignQuests()
    {
        checkIfDup();

        for (int i = 0; i < questSlots.Length; i++)
        {
            questSlots[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < CurrentQuests.Count; i++)
        {
            questSlots[i].quest = CurrentQuests[i];

            


            questSlots[i].gameObject.SetActive(true);
        }
    }

    public void DisplayQuestPopUp()
    {

    }

    public IEnumerator  PopUP()
    {
        questPopUp.SetActive(true);
        yield return new WaitForSeconds(3);
        questPopUp.SetActive(false);
    }

    void checkIfDup()
    {
        for (int i = 0; i < CurrentQuests.Count; i++)
        {
            for (int l = 0; l < CurrentQuests.Count; l++)
            {
                if(l != i)
                {
                    if(CurrentQuests[l] == CurrentQuests[i])
                    {
                        CurrentQuests.Remove(CurrentQuests[l]);
                    }
                }
            }
        }
    }

}
