﻿using UnityEngine;
using System.Collections;
using VIDE_Data; //Access VD class to retrieve node data
using UnityEngine.UI;

public class UI_Manager : MonoBehaviour
{
    public GameObject container_NPC;
    public GameObject container_PLAYER;
    public Text text_NPC;
    public Text text_Player;
    public Text text_NpcName;
    public Text[] text_Choices;
    

    public VIDE_Assign NPC;
    public bool isInConversation;
    // Use this for initialization
    void Start()
    {
        VD.LoadDialogues();
        container_NPC.SetActive(false);
        container_PLAYER.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Begin(VIDE_Assign assign)
    {
        isInConversation = true;
        VD.OnNodeChange += UpdateUI;
       // VD.OnEnd += End;
        VD.BeginDialogue(assign);
    }

    void UpdateUI(VD.NodeData data)
    {
        container_NPC.SetActive(false);
        container_PLAYER.SetActive(false);
        if (data.isPlayer)
        {
            container_PLAYER.SetActive(true);
            
                    
            text_Player.transform.parent.gameObject.SetActive(true);
            text_Player.text = data.comments[0];
                
          
            
        }
        else
        {
            container_NPC.SetActive(true);
            text_NpcName.text  = data.tag;
            text_NPC.text = data.comments[data.commentIndex];
            //Play Audio if any
            
        }
    }


public void End(VD.NodeData data)
{
    
    container_NPC.SetActive(false);
    container_PLAYER.SetActive(false);
    VD.OnNodeChange -= UpdateUI;
    VD.OnEnd -= End;
    VD.EndDialogue();
}

void OnDisable()
{
    if (container_NPC != null)
        End(null);
}

public void SetPlayerChoice(int choice)
{
    VD.nodeData.commentIndex = choice;
    if (Input.GetMouseButtonUp(0))
        VD.Next();
}


}
