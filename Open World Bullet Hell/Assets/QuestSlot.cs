﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestSlot : MonoBehaviour
{
    public Text questName;
    public Button button;
    public Quest quest;
    public GameObject display;
    public Text[] displayText;

    void Awake()
    {
        button = GetComponent<Button>();
        displayText = display.GetComponentsInChildren<Text>();
        questName = GetComponentInChildren<Text>();
    }

    void OnEnable()
    {
        if (quest != null)
        questName.text = quest.questName;
    }

    

    public void DisplayQuest()
    {
        display.gameObject.SetActive(true);
        displayText[0].text = quest.questName;
        displayText[1].text = MakeSummery(quest);
    }

    
    public int GetKillCount()
    {
        if (quest != null)
        {
            if (quest.EnemeyName == "Slime")
                return GameManager.gameInfo.SlimeQuestKillCount;
            else if (quest.EnemeyName == "Goblin")
                return GameManager.gameInfo.GoblinQuestKillCount;
            else if (quest.EnemeyName == "Orc")
                return GameManager.gameInfo.OrcQuestKillCount;
            else if (quest.EnemeyName == "Necromancer")
                return GameManager.gameInfo.NecroKillCount;
        }

        return 0;
    }

    public string MakeSummery(Quest quest)
    {
        string killcount = "";

        if (quest.EnemeyName == "Slime")
            killcount = GameManager.gameInfo.SlimeQuestKillCount.ToString();
        else if (quest.EnemeyName == "Goblin")
            killcount = GameManager.gameInfo.GoblinQuestKillCount.ToString();
        else if (quest.EnemeyName == "Orc")
            killcount = GameManager.gameInfo.OrcQuestKillCount.ToString();
        else if (quest.EnemeyName == "Slime King")
            killcount = GameManager.gameInfo.NecroKillCount.ToString();

        string progress = "Progress: " + killcount + " / " + quest.totalKillCount + " " + quest.EnemeyName + " Killed    " + CheckProgress() ;



        string summery = "Difficulty: " + quest.difficulty + "\n" + "\n" +
                         progress + "\n" + "\n" +
                         quest.Summery + "\n" +
                         quest.reward;

        return summery;
    }

    string CheckProgress()
    {
        if (GetKillCount() >= quest.totalKillCount)
        {
            return "Done! See the Steward.";
        }

        return null;
    }
}
