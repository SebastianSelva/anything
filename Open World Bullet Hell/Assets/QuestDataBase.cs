﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif


[CreateAssetMenu]
public class QuestDataBase: ScriptableObject {

    [SerializeField] Quest[] quests;

    public Quest GetItemReference(string itemID)
    {
        foreach (Quest quest in quests)
        {
            if (quest.ID == itemID)
            {
                return quest;
            }
        }
        return null;
    }


    public Quest GetItemCopy(string questID)
    {
        Quest quest = GetItemReference(questID);
        return quest != null ? quest.GetCopy() : null;
    }

#if UNITY_EDITOR

    private void OnValidate()
    {
        LoadQuest();
    }

    private void OnEnable()
    {
        EditorApplication.projectChanged -= LoadQuest;
        EditorApplication.projectChanged += LoadQuest;
    }

    private void OnDisable()
    {
        EditorApplication.projectChanged -= LoadQuest;
    }

    private void LoadQuest()
    {
        quests = FindAssetsByType<Quest>("Assets/Quests");
    }

    public static T[] FindAssetsByType<T>(params string[] folders) where T : Object
    {
        string type = typeof(T).ToString().Replace("UnityEngine.", "");

        string[] guids;
        if (folders == null || folders.Length == 0)
        {
            guids = AssetDatabase.FindAssets("t:" + type);
        }
        else
        {
            guids = AssetDatabase.FindAssets("t:" + type, folders);
        }

        T[] assets = new T[guids.Length];

        for (int i = 0; i < guids.Length; i++)
        {
            string assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
            assets[i] = AssetDatabase.LoadAssetAtPath<T>(assetPath);
        }
        return assets;
    }
#endif
}
