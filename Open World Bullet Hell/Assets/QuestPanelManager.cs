﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestPanelManager : MonoBehaviour
{
    public GameObject display;
    public Text[] texts;

    void Awake()
    {
        gameObject.SetActive(false);
        texts = display.GetComponentsInChildren<Text>();
    }
	
    void OnEnable()
    {
        GameManager.questManager.AssignQuests();
        display.gameObject.SetActive(false);
    }

    void OnDisable()
    {
        if (texts.Length > 0)
        {
            texts[0].text = "";
            texts[1].text = "";
        }
    }


}
