﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootPickUp : MonoBehaviour
{
    public List<LootBag> LootBagsNearBy;

    void OnTriggerEnter(Collider Col)
    {
        if(Col.tag == "LootBag")
        {
            LootBagsNearBy.Add(Col.GetComponent<LootBag>());
        }
    }

    void OnTriggerExit(Collider Col)
    {
        if(Col.tag == "LootBag" && LootBagsNearBy.Contains(Col.GetComponent<LootBag>()))
        {
            LootBagsNearBy.Remove(Col.GetComponent<LootBag>());
        }
    }
	
}
