﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonSpawner : MonoBehaviour {

    public GameObject SkeletonPrefab;
    public GameObject CurrentSkeleton;
    public float SpawnTimer;
    public bool playerNear;
    void Awake()
    {
        SpawnSlime();
    }

    void Update()
    {
        if (CurrentSkeleton == null)
        {
            SpawnTimer -= Time.deltaTime;
            if (SpawnTimer <= 0)
            {
                SpawnSlime();
            }
        }

        if (playerNear == false && CurrentSkeleton != null)
            CurrentSkeleton.SetActive(false);
        else if (playerNear == true && CurrentSkeleton != null)
            CurrentSkeleton.SetActive(true);

    }


    public void SpawnSlime()
    {
        GameObject Slime = Instantiate(SkeletonPrefab, this.transform.position, Quaternion.identity);
        CurrentSkeleton = Slime;
        SpawnTimer = 60;
    }

    private void OnTriggerEnter(Collider Col)
    {
        if (Col.tag == "Activator")
        {
            playerNear = true;
        }

    }

    private void OnTriggerExit(Collider Col)
    {
        if (Col.tag == "Activator")
        {
            playerNear = false;
        }

    }
}
