﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoblinSpawner : MonoBehaviour
{
    public GameObject GoblinPrefab;
    public GameObject CurrentGoblin;
    public float SpawnTimer;
    public bool playerNear;
    void Awake()
    {
        SpawnGoblin();
    }

    void Update()
    {
        if(CurrentGoblin == null)
        {
            SpawnTimer -= Time.deltaTime;
            if (SpawnTimer <= 0)
            {
                SpawnGoblin();
            }
        }

        if (playerNear == false && CurrentGoblin != null)
            CurrentGoblin.SetActive(false);
        else if(playerNear == true && CurrentGoblin != null)
            CurrentGoblin.SetActive(true);
        
    }


    public void SpawnGoblin()
    {
        GameObject goblin =  Instantiate(GoblinPrefab, this.transform.position, Quaternion.identity);
        CurrentGoblin = goblin;
        SpawnTimer = 60;
    }

    private void OnTriggerEnter(Collider Col)
    {
        if(Col.tag == "Activator")
        {
            playerNear = true;
        }
        
    }

    private void OnTriggerExit(Collider Col)
    {
        if (Col.tag == "Activator")
        {
            playerNear = false;
        }

    }




}
