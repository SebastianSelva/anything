﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VIDE_Data;

public class StewardDialogueScript: MonoBehaviour
{
    
    public VIDE_Assign VIDE;

    public Quest FirstQuest;
    public Quest SecondQuest;
    public Quest ThirdQuest;
    public Quest FourthQuest;
    
    
    private void Awake()
    {
        
    }

    void Update ()
    {
        
	}

    public void addFirstQuest()
    {
        GameManager.questManager.CurrentQuests.Add(FirstQuest);
        GameManager.gameInfo.IsOnSlimeQuest = true;
        StartCoroutine(GameManager.questManager.PopUP());
    }

    public void addSecondQuest()
    {
        GameManager.questManager.CurrentQuests.Add(SecondQuest);
        GameManager.gameInfo.IsOnGoblinQuest = true;
        StartCoroutine(GameManager.questManager.PopUP());
    }

    public void addThirdQuest()
    {
        GameManager.questManager.CurrentQuests.Add(ThirdQuest);
        GameManager.gameInfo.IsOnOrcQuest = true;
        StartCoroutine(GameManager.questManager.PopUP());
    }

    public void addFourthQuest()
    {
        GameManager.questManager.CurrentQuests.Add(FourthQuest);
        GameManager.gameInfo.IsOnOrcQuest = true;
        StartCoroutine(GameManager.questManager.PopUP());
    }


    public void talkedToKing()
    {
        GameManager.gameInfo.KingFisrtTime = true;
        VIDE.overrideStartNode = 2;
        GameManager.GM.talkToKing.SetActive(false);
    }

    public void  CheckForSlimeCompletion()
    {
        for (int i = 0; i < GameManager.questManager.questSlots.Length; i++)
        {
            if(GameManager.questManager.questSlots[i].quest.EnemeyName == "Slime")
            {
                if (GameManager.questManager.questSlots[i].GetKillCount() >= GameManager.questManager.questSlots[i].quest.totalKillCount)
                {
                    VD.SetNode(12);
                }
            }
            
        }
        
    }

    public void CheckForGoblinCompletion()
    {
        for (int i = 0; i < GameManager.questManager.questSlots.Length; i++)
        {
            if (GameManager.questManager.questSlots[i].quest.EnemeyName == "Goblin")
            {
                if (GameManager.questManager.questSlots[i].GetKillCount() >= GameManager.questManager.questSlots[i].quest.totalKillCount)
                {
                    VD.SetNode(19);
                }
            }

        }

    }

    public void CheckForOrcCompletion()
    {
        for (int i = 0; i < GameManager.questManager.questSlots.Length; i++)
        {
            if (GameManager.questManager.questSlots[i].quest.EnemeyName == "Orc")
            {
                if (GameManager.questManager.questSlots[i].GetKillCount() >= GameManager.questManager.questSlots[i].quest.totalKillCount)
                {
                    VD.SetNode(26);
                }
            }

        }

    }

    public void CheckForNecroCompletion()
    {
        for (int i = 0; i < GameManager.questManager.questSlots.Length; i++)
        {
            if (GameManager.questManager.questSlots[i].quest.EnemeyName == "Necromancer")
            {
                if (GameManager.questManager.questSlots[i].GetKillCount() >= GameManager.questManager.questSlots[i].quest.totalKillCount)
                {
                    VD.SetNode(33);
                }
            }

        }

    }

    public static ActionNode QuestComplete;
   

}
