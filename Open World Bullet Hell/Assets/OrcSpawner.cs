﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrcSpawner : MonoBehaviour
{

    public GameObject OrcPrefab;
    public GameObject CurrentOrc;
    public float SpawnTimer;
    public bool playerNear;
    void Awake()
    {
        SpawnOrc();
    }

    void Update()
    {
        if (CurrentOrc == null)
        {
            SpawnTimer -= Time.deltaTime;
            if (SpawnTimer <= 0)
            {
                SpawnOrc();
            }
        }

        if (playerNear == false && CurrentOrc != null)
            CurrentOrc.SetActive(false);
        else if (playerNear == true && CurrentOrc != null)
            CurrentOrc.SetActive(true);

    }


    public void SpawnOrc()
    {
        GameObject Orc = Instantiate(OrcPrefab, this.transform.position, Quaternion.identity);
        CurrentOrc = Orc;
        SpawnTimer = 60;
    }

    private void OnTriggerEnter(Collider Col)
    {
        if (Col.tag == "Activator")
        {
            playerNear = true;
        }

    }

    private void OnTriggerExit(Collider Col)
    {
        if (Col.tag == "Activator")
        {
            playerNear = false;
        }

    }
}
